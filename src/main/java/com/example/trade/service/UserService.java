package com.example.trade.service;

import com.example.trade.dto.UserHistoryDto;

public interface UserService {
	UserHistoryDto searchUserHistory(Long userId);
}
