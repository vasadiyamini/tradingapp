package com.example.trade.service;

import com.example.trade.dto.StockPricesDisplay;

public interface CurrentStockPriceService {

	StockPricesDisplay getCurrentPriceStock(Long stockId);

}
