package com.example.trade.service;

import java.util.List;

import com.example.trade.dto.StockPricesDisplay;

public interface PastStocksPriceService {

	List<StockPricesDisplay> getStockPrices(Long stockId);

}
