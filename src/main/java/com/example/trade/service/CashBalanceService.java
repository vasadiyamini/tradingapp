package com.example.trade.service;

import org.springframework.stereotype.Service;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.CashBalanceDto;

@Service
public interface CashBalanceService {

	ApiResponse cashBalanced(Long userId, CashBalanceDto cashBalanceDto);
}
