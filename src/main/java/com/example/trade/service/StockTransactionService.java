package com.example.trade.service;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.TransactionDto;
import com.example.trade.entity.TransactionType;

public interface StockTransactionService {

	ApiResponse buyStock(TransactionDto transactionDto, TransactionType transactionType);
}
