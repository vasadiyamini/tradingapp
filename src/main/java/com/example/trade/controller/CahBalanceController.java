package com.example.trade.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.CashBalanceDto;
import com.example.trade.service.CashBalanceService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")

@RequiredArgsConstructor

public class CahBalanceController {

	private final CashBalanceService cashBalanceService;

	@PutMapping("/balance/{userId}/cashbalance")
	/**
	 * 
	 * @param userId
	 * @param cashBalanceDto
	 * @return
	 */
	public ResponseEntity<ApiResponse> creadiBalnced(@PathVariable("userId") @NotNull Long userId,
			@Valid @RequestBody CashBalanceDto cashBalanceDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(cashBalanceService.cashBalanced(userId, cashBalanceDto));
	}

}
