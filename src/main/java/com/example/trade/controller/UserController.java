package com.example.trade.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trade.dto.UserHistoryDto;
import com.example.trade.service.UserService;

import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")

public class UserController {

	private final UserService userService;

	/**
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/users/{userId}")
	public ResponseEntity<UserHistoryDto> searchHistory(@PathVariable("userId") @NotNull Long userId) {
		return new ResponseEntity<>(userService.searchUserHistory(userId), HttpStatus.OK);
	}

}
