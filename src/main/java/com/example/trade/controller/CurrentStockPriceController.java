package com.example.trade.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trade.dto.StockPricesDisplay;
import com.example.trade.service.CurrentStockPriceService;

import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class CurrentStockPriceController {
	private final CurrentStockPriceService currentStockPriceService;

	/**
	 * 
	 * @param stockId will give the stock details
	 * @return getting for current price
	 */
	@GetMapping("/stocksvalue/{stockId}")
	ResponseEntity<StockPricesDisplay> getCurrentPriceStock(@PathVariable("stockId") @NotNull Long stockId) {
		log.info("Current Stock price fetched successfully");
		return ResponseEntity.status(HttpStatus.OK).body(currentStockPriceService.getCurrentPriceStock(stockId));
	}

}
