package com.example.trade.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.TransactionDto;
import com.example.trade.entity.TransactionType;
import com.example.trade.service.StockTransactionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class StockTransactionController {
	private final StockTransactionService stockTransactionService;

	@PostMapping("/stocktransactions")
	/**
	 * 
	 * @param transactionDto
	 * @param transactionType
	 * @return
	 */
	public ResponseEntity<ApiResponse> buyStock(@RequestBody @Valid TransactionDto transactionDto,
			@RequestParam TransactionType transactionType) {
		return new ResponseEntity<>(stockTransactionService.buyStock(transactionDto, transactionType),
				HttpStatus.CREATED);
	}
}
