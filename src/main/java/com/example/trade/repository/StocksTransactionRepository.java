package com.example.trade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trade.entity.StocksTransaction;
import com.example.trade.entity.User;

public interface StocksTransactionRepository extends JpaRepository<StocksTransaction, Long> {
	List<StocksTransaction> findByUser(User user);
}
