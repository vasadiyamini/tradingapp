package com.example.trade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trade.entity.Stock;

public interface StockRepository extends JpaRepository<Stock, Long> {

}
