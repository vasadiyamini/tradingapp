package com.example.trade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trade.entity.Trade;
import com.example.trade.entity.User;

public interface TradeRepository extends JpaRepository<Trade, Long> {
	List<Trade> findByUser(User user);
}
