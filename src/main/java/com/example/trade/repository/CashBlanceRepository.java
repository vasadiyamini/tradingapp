package com.example.trade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trade.entity.CashBlance;
import com.example.trade.entity.User;

public interface CashBlanceRepository extends JpaRepository<CashBlance, Long> {
	CashBlance findByUser(User user);
}
