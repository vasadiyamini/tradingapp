package com.example.trade.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trade.entity.Stock;
import com.example.trade.entity.StockPrice;

public interface StockPriceRepository extends JpaRepository<StockPrice, Long> {

	StockPrice findByStockAndDate(Stock stock, LocalDate date);

	List<StockPrice> findTop5ByStockOrderByDateDesc(Stock stock);

	Optional<StockPrice> findFirstByStockOrderByDateDesc(Stock stock);

}
