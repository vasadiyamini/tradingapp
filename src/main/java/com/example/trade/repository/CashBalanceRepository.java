package com.example.trade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trade.entity.CashBlance;

public interface CashBalanceRepository extends JpaRepository<CashBlance, Long> {

}
