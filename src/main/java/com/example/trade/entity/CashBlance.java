package com.example.trade.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CashBlance {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cashBalanceId;
	@OneToOne(cascade = CascadeType.ALL)
	private User user;
	private Double balance;
}
