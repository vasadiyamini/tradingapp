package com.example.trade.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class StocksTransaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long orderId;
	@ManyToOne(cascade = CascadeType.ALL)
	private User user;
	private int shares;
	@Enumerated(EnumType.STRING)
	private Status staus;
	@ManyToOne(cascade = CascadeType.ALL)
	private Stock stock;
	private Double totalCost;
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;

}
