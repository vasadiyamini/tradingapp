package com.example.trade.entity;

public enum TransactionType {
	BUY, SELL;
}
