package com.example.trade.exception;

public class InvalidTransactionType extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTransactionType() {
		super("Invalid Transaction Type", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public InvalidTransactionType(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
