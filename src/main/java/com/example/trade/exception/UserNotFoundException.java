package com.example.trade.exception;

public class UserNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFoundException() {
		super("User Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public UserNotFoundException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
