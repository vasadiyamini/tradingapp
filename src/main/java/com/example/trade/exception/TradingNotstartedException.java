package com.example.trade.exception;

public class TradingNotstartedException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TradingNotstartedException() {
		super("Trading not started", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public TradingNotstartedException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}
}