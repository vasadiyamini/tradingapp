package com.example.trade.exception;

public class StockNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StockNotFoundException() {

		super("Stock id not exist", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public StockNotFoundException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
