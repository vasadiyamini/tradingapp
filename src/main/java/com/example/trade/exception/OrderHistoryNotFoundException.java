package com.example.trade.exception;

public class OrderHistoryNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderHistoryNotFoundException() {
		super("ORDER NOT FOUND", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public OrderHistoryNotFoundException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
