package com.example.trade.exception;

public class InsufficientCashBalance extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientCashBalance() {
		super("Insufficient Cash Balance", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public InsufficientCashBalance(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
