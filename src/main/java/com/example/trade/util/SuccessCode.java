package com.example.trade.util;

public class SuccessCode {
	public static final String ADD_CASH_SUCCESSFUL = "Cash balance updated successfully";
	public static final String ORDER_PLACED_SUCCESSFULLY = "Shares purchased successfully";
	public static final String SUCCESS_CODE1 = "SUCCESS1";
	public static final String SUCCESS_CODE2 = "SUCCESS2";

	private SuccessCode() {
		super();
	}
}
