package com.example.trade.dto;

import lombok.Builder;

@Builder
public record TradeHistoryDto(String companyName, Long bse_code, Double pe, Long market_cap, Double change_pct

) {

}
