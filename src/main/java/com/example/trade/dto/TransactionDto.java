package com.example.trade.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record TransactionDto(@NotNull Long userId, @NotNull Long stockId,
		@Min(value = 1, message = "minimum 1") @Max(value = 10, message = "maximum 10") Integer noOfShares) {

}