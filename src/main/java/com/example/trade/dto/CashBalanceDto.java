package com.example.trade.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CashBalanceDto {
	@Min(value = 1, message = "minimum 1")
	@Max(value = 10000, message = "maximum 10000")
	private Double balance;
}
