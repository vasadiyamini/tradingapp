package com.example.trade.dto;

import com.example.trade.entity.Status;

import lombok.Builder;

@Builder
public record OrderHistoryDto(String CompanyName, Integer shares, Status status, Double totalCost) {

}
