package com.example.trade.dto;

import java.time.LocalDate;

public record StockPricesDisplay(String stockName, Double Price, LocalDate date) {

}
