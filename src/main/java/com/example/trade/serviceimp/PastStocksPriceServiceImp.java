package com.example.trade.serviceimp;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.trade.dto.StockPricesDisplay;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StockPrice;
import com.example.trade.exception.StockNotFoundException;
import com.example.trade.exception.TradingNotstartedException;
import com.example.trade.repository.StockPriceRepository;
import com.example.trade.repository.StockRepository;
import com.example.trade.service.PastStocksPriceService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
/**
 * Implementation of fetching stock price
 */
public class PastStocksPriceServiceImp implements PastStocksPriceService {
	private final StockRepository stockRepository;
	private final StockPriceRepository stockPriceRepository;

	/**
	 * Get stock Using stock Id
	 */
	@Override
	public List<StockPricesDisplay> getStockPrices(Long stockId) {
		log.warn("Stock not found");
		Stock stock = stockRepository.findById(stockId).orElseThrow(StockNotFoundException::new);
		String name = stock.getCompany();
		List<StockPrice> stockPrice = stockPriceRepository.findTop5ByStockOrderByDateDesc(stock);
		if (stockPrice.isEmpty()) {
			log.warn("Trading not started");
			throw new TradingNotstartedException();
		}
		log.info("Details fetched successfully");
		return stockPrice.stream().map(sp -> new StockPricesDisplay(name, sp.getPrice(), sp.getDate())).toList();

	}

}
