package com.example.trade.serviceimp;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.TransactionDto;
import com.example.trade.entity.CashBlance;
import com.example.trade.entity.Status;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StockPrice;
import com.example.trade.entity.StocksTransaction;
import com.example.trade.entity.Trade;
import com.example.trade.entity.TransactionType;
import com.example.trade.entity.User;
import com.example.trade.exception.InsufficientCashBalance;
import com.example.trade.exception.InvalidTransactionType;
import com.example.trade.exception.StockNotFoundException;
import com.example.trade.exception.UserNotFoundException;
import com.example.trade.repository.CashBlanceRepository;
import com.example.trade.repository.StockPriceRepository;
import com.example.trade.repository.StockRepository;
import com.example.trade.repository.StocksTransactionRepository;
import com.example.trade.repository.TradeRepository;
import com.example.trade.repository.UserRepository;
import com.example.trade.service.StockTransactionService;
import com.example.trade.util.SuccessCode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
/*
 * Implementation of Stock Transaction
 */
public class StockTransactionServiceImpl implements StockTransactionService {
	private final UserRepository userRepository;
	private final StockRepository stockRepository;
	private final StockPriceRepository stockPriceRepository;
	private final CashBlanceRepository cashBlanceRepository;
	private final StocksTransactionRepository stocksTransactionRepository;
	private final TradeRepository tradeRepository;

	/*
	 * Using transactionDto and transactionType to buy stock check validate
	 * userId,stockId and balance
	 */
	@Override
	public ApiResponse buyStock(TransactionDto transactionDto, TransactionType transactionType) {
		
		User user = userRepository.findById(transactionDto.userId()).orElseThrow(() -> {
			log.error("invalid userId caused UserNotFoundException");
			throw new UserNotFoundException();
		});
		if (transactionType.equals(TransactionType.SELL))
			throw new InvalidTransactionType();
		
		Stock stock = stockRepository.findById(transactionDto.stockId()).orElseThrow(() -> {
			log.error("invalid stockId caused StockNotFoundException");
			throw new StockNotFoundException();
		});

		StockPrice stockPrice = stockPriceRepository.findByStockAndDate(stock, LocalDate.now());

		CashBlance cashAccount = cashBlanceRepository.findByUser(user);

		Double totalPrice = transactionDto.noOfShares() * stockPrice.getPrice();

		StocksTransaction transaction = StocksTransaction.builder().shares(transactionDto.noOfShares())
				.staus(Status.REJECTED).stock(stock).totalCost(totalPrice).transactionType(transactionType).user(user)
				.build();
		stocksTransactionRepository.save(transaction);
		if(cashAccount.getBalance()<totalPrice){
			log.warn("InSufficient Balance");
			throw new InsufficientCashBalance();
		}
		
			log.info("Sufficient Balance Available");
			transaction.setStaus(Status.SUCCESS);
			stocksTransactionRepository.save(transaction);
			Trade trade = Trade.builder().buyDate(LocalDate.now()).stock(stock).order(transaction).user(user).build();
			tradeRepository.save(trade);
			cashAccount.setBalance(cashAccount.getBalance() - totalPrice);
			cashBlanceRepository.save(cashAccount);
		 
		log.info("Placed Order Successfully");
		return ApiResponse.builder().message(SuccessCode.ORDER_PLACED_SUCCESSFULLY).code(SuccessCode.SUCCESS_CODE2)
				.build();
	}

}
