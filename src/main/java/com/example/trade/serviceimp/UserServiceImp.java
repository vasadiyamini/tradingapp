package com.example.trade.serviceimp;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.trade.dto.OrderHistoryDto;
import com.example.trade.dto.TradeHistoryDto;
import com.example.trade.dto.UserHistoryDto;
import com.example.trade.entity.CashBlance;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StocksTransaction;
import com.example.trade.entity.Trade;
import com.example.trade.entity.User;
import com.example.trade.exception.OrderHistoryNotFoundException;
import com.example.trade.exception.UserNotFoundException;
import com.example.trade.repository.CashBlanceRepository;
import com.example.trade.repository.StocksTransactionRepository;
import com.example.trade.repository.TradeRepository;
import com.example.trade.repository.UserRepository;
import com.example.trade.service.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
/**
 * Implementation of user service to fetch search
 */
public class UserServiceImp implements UserService {

	private final UserRepository userRepository;
	private final CashBlanceRepository cashBlanceRepository;
	private final StocksTransactionRepository stocksTransactionRepository;
	private final TradeRepository tradeRepository;

	@Override
	/**
	 * using UserId read or fetch the data of Account_balance, Order_Details,
	 * Trading_Details and Market_Price first we check the user-Id valid or not
	 */

	public UserHistoryDto searchUserHistory(Long userId) {

		User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
		List<StocksTransaction> stocksTransactions = stocksTransactionRepository.findByUser(user);
		if (stocksTransactions.isEmpty()) {
			log.warn("Order History Not Found");
			throw new OrderHistoryNotFoundException();
		}
		Map<StocksTransaction, Stock> stocksTransactionMap = stocksTransactions.stream()
				.collect(Collectors.toMap(Function.identity(), StocksTransaction::getStock));
		log.info("Create Order History Dto");
		List<OrderHistoryDto> orderHistoryDtos = stocksTransactions.stream().map(stocksTransaction -> {
			Stock stock = stocksTransactionMap.get(stocksTransaction);
			return new OrderHistoryDto(stock.getCompany(), stocksTransaction.getShares(), stocksTransaction.getStaus(),
					stocksTransaction.getTotalCost());

		}).toList();

		CashBlance cashBlance = cashBlanceRepository.findByUser(user);
		List<Trade> trades = tradeRepository.findByUser(user);

		Map<Trade, Stock> tradeMap = trades.stream().collect(Collectors.toMap(Function.identity(), Trade::getStock));
		log.info("Create trade History Dto");
		List<TradeHistoryDto> tradeHistoryDtos = trades.stream().map(trade -> {
			Stock stock = tradeMap.get(trade);
			return new TradeHistoryDto(stock.getCompany(), stock.getBseCode(), stock.getPe(), stock.getMarketCap(),
					stock.getChangePct());

		}).toList();

		log.info("Fetch User History");
		return new UserHistoryDto(cashBlance.getBalance(), orderHistoryDtos, tradeHistoryDtos);
	}

}
