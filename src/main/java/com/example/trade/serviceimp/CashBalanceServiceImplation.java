package com.example.trade.serviceimp;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.CashBalanceDto;
import com.example.trade.entity.CashBlance;
import com.example.trade.exception.ResourceNotFound;
import com.example.trade.repository.CashBalanceRepository;
import com.example.trade.service.CashBalanceService;
import com.example.trade.util.SuccessCode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
/*
 * Implementation of Cash balance Updation
 */
public class CashBalanceServiceImplation implements CashBalanceService {

	private final CashBalanceRepository cashBalanceRepository;

	/**
	 * Use userId and balance to update cash balance validate userId and fetch user
	 * cashBalance
	 */
	@Override
	public ApiResponse cashBalanced(Long userId, CashBalanceDto cashBalanceDto) {

		Optional<CashBlance> cashBalance1 = cashBalanceRepository.findById(userId);
		if (cashBalance1.isPresent()) {
			log.info("Cash Balance for User Found");
			CashBlance cashBlances = cashBalance1.get();
			cashBlances.setBalance(cashBalanceDto.getBalance() + cashBlances.getBalance());
			cashBalanceRepository.save(cashBlances);
			ApiResponse api = new ApiResponse();
			api.setCode(SuccessCode.SUCCESS_CODE1);
			api.setMessage(SuccessCode.ADD_CASH_SUCCESSFUL);
			log.info("Successfully updated");
			return api;
		} else {
			log.warn("userid is not present");
			throw new ResourceNotFound("userid is not present");

		}
	}
}
