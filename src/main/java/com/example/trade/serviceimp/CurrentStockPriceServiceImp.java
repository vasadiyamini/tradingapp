package com.example.trade.serviceimp;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.trade.dto.StockPricesDisplay;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StockPrice;
import com.example.trade.exception.StockNotFoundException;
import com.example.trade.exception.TradingNotstartedException;
import com.example.trade.repository.StockPriceRepository;
import com.example.trade.repository.StockRepository;
import com.example.trade.service.CurrentStockPriceService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
/**
 * Implementation of getting current price
 */
public class CurrentStockPriceServiceImp implements CurrentStockPriceService {
	private final StockRepository stockRepository;
	private final StockPriceRepository stockPriceRepository;

	/**
	 * validate stock Id and fetch current stock price
	 */
	@Override
	public StockPricesDisplay getCurrentPriceStock(Long stockId) {
		Stock stock = stockRepository.findById(stockId).orElseThrow(StockNotFoundException::new);
		Optional<StockPrice> stockPrice = stockPriceRepository.findFirstByStockOrderByDateDesc(stock);
		String name = stock.getCompany();
		if (stockPrice.isEmpty()) {
			log.warn("Trading not started");
			throw new TradingNotstartedException();
		}
		StockPrice price = stockPrice.get();
		log.info("Fetched Current Market Price");
		return new StockPricesDisplay(name, price.getPrice(), price.getDate());
	}

}
