package com.example.trade.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.trade.dto.UserHistoryDto;
import com.example.trade.entity.CashBlance;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StocksTransaction;
import com.example.trade.entity.Trade;
import com.example.trade.entity.TransactionType;
import com.example.trade.entity.User;
import com.example.trade.exception.OrderHistoryNotFoundException;
import com.example.trade.repository.CashBlanceRepository;
import com.example.trade.repository.StocksTransactionRepository;
import com.example.trade.repository.TradeRepository;
import com.example.trade.repository.UserRepository;
import com.example.trade.serviceimp.UserServiceImp;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	CashBlanceRepository cashBlanceRepository;
	@Mock
	StocksTransactionRepository stocksTransactionRepository;
	@Mock
	TradeRepository tradeRepository;
	@InjectMocks
	UserServiceImp userServiceImp;

	@Test
	void testSearchUserHistorySuccess() {

		User user = User.builder().userId(1L).build();
		Stock stock = Stock.builder().company("HCL Tech").bseCode(10L).changePct(10).marketCap(1L).pe(1D).stockid(1L)
				.build();
		Long userId = 1L;
		StocksTransaction stocksTransaction = StocksTransaction.builder().orderId(1L).user(user).stock(stock).shares(10)
				.transactionType(TransactionType.BUY).totalCost(1000D).build();
		CashBlance cashBlance = CashBlance.builder().cashBalanceId(1L).balance(100D).user(user).build();
		List<StocksTransaction> stocksTransactions = Arrays.asList(stocksTransaction);
		List<Trade> trades = Arrays
				.asList(Trade.builder().stock(stock).user(user).tradeId(1L).order(stocksTransaction).build());

		Mockito.when(cashBlanceRepository.findByUser(user)).thenReturn(cashBlance);
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		Mockito.when(tradeRepository.findByUser(user)).thenReturn(trades);
		Mockito.when(stocksTransactionRepository.findByUser(user)).thenReturn(stocksTransactions);

		UserHistoryDto userHistoryDto = userServiceImp.searchUserHistory(userId);

		assertEquals(100D, userHistoryDto.cashBalance());
	}

	@Test
	void testSearchUserHistoryFailure() {
		User user = User.builder().userId(1L).build();
		Long userId = 1L;

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		Mockito.when(stocksTransactionRepository.findByUser(user)).thenReturn(Collections.emptyList());

		assertThrows(OrderHistoryNotFoundException.class, () -> userServiceImp.searchUserHistory(userId));
	}
}
