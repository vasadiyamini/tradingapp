package com.example.trade.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.trade.dto.StockPricesDisplay;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StockPrice;
import com.example.trade.exception.StockNotFoundException;
import com.example.trade.exception.TradingNotstartedException;
import com.example.trade.repository.StockPriceRepository;
import com.example.trade.repository.StockRepository;
import com.example.trade.serviceimp.CurrentStockPriceServiceImp;

@ExtendWith(MockitoExtension.class)

class CurrentStockPriceServiceImpTest {
	@Mock
	StockRepository stockRepository;

	@Mock
	StockPriceRepository stockPriceRepository;

	@InjectMocks
	CurrentStockPriceServiceImp currentStockPriceService;

	@Test
	void testGetCurrentPriceStockSuccess() {
		Stock mockStock = new Stock(1L, "ABC Company", 123L, 10.0, 100000L, 1.0);
		StockPrice mockStockPrice = new StockPrice(1L, mockStock, 100.0, LocalDate.now());
		when(stockRepository.findById(1L)).thenReturn(Optional.of(mockStock));
		when(stockPriceRepository.findFirstByStockOrderByDateDesc(mockStock)).thenReturn(Optional.of(mockStockPrice));

		StockPricesDisplay result = currentStockPriceService.getCurrentPriceStock(1L);

		assertEquals("ABC Company", result.stockName());
		assertEquals(100.0, result.Price());
		assertEquals(LocalDate.now(), result.date());
	}

	@Test
	void testGetCurrentPriceStockStockNotFound() {
		when(stockRepository.findById(1L)).thenReturn(Optional.empty());

		assertThrows(StockNotFoundException.class, () -> currentStockPriceService.getCurrentPriceStock(1L));
	}

	@Test
	void testGetCurrentPriceStockTradingNotStarted() {
		Stock mockStock = new Stock(1L, "ABC Company", 123L, 10.0, 100000L, 1.0);
		when(stockRepository.findById(1L)).thenReturn(Optional.of(mockStock));
		when(stockPriceRepository.findFirstByStockOrderByDateDesc(mockStock)).thenReturn(Optional.empty());

		assertThrows(TradingNotstartedException.class, () -> currentStockPriceService.getCurrentPriceStock(1L));
	}

	@Test
	void testGetCurrentPriceStockNullStockPrice() {
		Stock mockStock = new Stock(1L, "ABC Company", 123L, 10.0, 100000L, 1.0);
		when(stockRepository.findById(1L)).thenReturn(Optional.of(mockStock));
		when(stockPriceRepository.findFirstByStockOrderByDateDesc(mockStock)).thenReturn(Optional.empty());

		Exception exception = assertThrows(TradingNotstartedException.class,
				() -> currentStockPriceService.getCurrentPriceStock(1L));

		assertEquals("Trading not started", exception.getMessage());
	}
}
