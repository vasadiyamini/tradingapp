package com.example.trade.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.TransactionDto;
import com.example.trade.entity.CashBlance;
import com.example.trade.entity.Status;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StockPrice;
import com.example.trade.entity.StocksTransaction;
import com.example.trade.entity.Trade;
import com.example.trade.entity.TransactionType;
import com.example.trade.entity.User;
import com.example.trade.exception.InsufficientCashBalance;
import com.example.trade.exception.InvalidTransactionType;
import com.example.trade.exception.StockNotFoundException;
import com.example.trade.exception.UserNotFoundException;
import com.example.trade.repository.CashBlanceRepository;
import com.example.trade.repository.StockPriceRepository;
import com.example.trade.repository.StockRepository;
import com.example.trade.repository.StocksTransactionRepository;
import com.example.trade.repository.TradeRepository;
import com.example.trade.repository.UserRepository;
import com.example.trade.serviceimp.StockTransactionServiceImpl;

@ExtendWith(SpringExtension.class)
class StockTransactionServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	StockRepository stockRepository;
	@Mock
	StockPriceRepository stockPriceRepository;
	@Mock
	TradeRepository tradeRepository;
	@Mock
	StocksTransactionRepository transactionRepository;
	@Mock
	CashBlanceRepository cashAccountRepository;

	@InjectMocks
	StockTransactionServiceImpl transactionServiceImpl;

	@Test
	void testPurchaseShare() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		User user = User.builder().email("vamsha@hclTech.com").userId(1l).name("Vamsha").build();
		Stock stock = Stock.builder().bseCode(100l).changePct(10.5).company("HCLTech").marketCap(1000l).pe(10.5).stockid(1l)
				.build();
		StockPrice stockPrice = StockPrice.builder().date(LocalDate.now()).stock(stock).stockPriceId(1l).price(234.0)
				.build();
		CashBlance cashAccount = CashBlance.builder().balance(1200.0).cashBalanceId(1l).user(user).build();
		StocksTransaction transaction = StocksTransaction.builder().shares(2).staus(Status.REJECTED).stock(stock)
				.totalCost(468.0).orderId(1l).transactionType(TransactionType.BUY).user(user).build();
		StocksTransaction transaction2 = StocksTransaction.builder().shares(2).staus(Status.SUCCESS).stock(stock)
				.totalCost(468.0).orderId(1l).transactionType(TransactionType.BUY).user(user).build();
		Trade trade = Trade.builder().buyDate(LocalDate.now()).stock(stock).tradeId(1l).order(transaction2).user(user)
				.build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(stockRepository.findById(transactionDto.stockId())).thenReturn(Optional.of(stock));
		Mockito.when(stockPriceRepository.findByStockAndDate(stock, LocalDate.now())).thenReturn(stockPrice);
		Mockito.when(cashAccountRepository.findByUser(user)).thenReturn(cashAccount);
		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		Mockito.when(transactionRepository.save(transaction2)).thenReturn(transaction2);
		Mockito.when(tradeRepository.save(trade)).thenReturn(trade);
		Mockito.when(cashAccountRepository.save(cashAccount)).thenReturn(cashAccount);
		ApiResponse apiResponse = transactionServiceImpl.buyStock(transactionDto, TransactionType.BUY);
		assertNotNull(apiResponse);
		assertEquals("Shares purchased successfully", apiResponse.getMessage());
	}

	@Test
	void testPurchaseInvalid1() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		User user = User.builder().email("vamsha@hclTech.com").userId(1l).name("Vamsha").build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(user));
		assertThrows(InvalidTransactionType.class,
				() -> transactionServiceImpl.buyStock(transactionDto, TransactionType.SELL));
	}

	@Test
	void testPurchaseInvalid2() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		Mockito.when(userRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(UserNotFoundException.class,
				() -> transactionServiceImpl.buyStock(transactionDto, TransactionType.BUY));
	}

	@Test
	void testPurchaseInvalid3() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(1).stockId(1l).userId(1l).build();
		User user = User.builder().email("vamsha@hclTech.com").userId(1l).name("Vamsha").build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(stockRepository.findById(transactionDto.stockId())).thenReturn(Optional.empty());
		assertThrows(StockNotFoundException.class,
				() -> transactionServiceImpl.buyStock(transactionDto, TransactionType.BUY));
	}

	@Test
	void testPurchaseInvalid4() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(1).stockId(1l).userId(1l).build();
		User user = User.builder().email("vamsha@hclTech.com").userId(1l).name("Vamsha").build();
		Stock stock = Stock.builder().bseCode(100l).changePct(10.5).company("HCLTech").marketCap(1000l).pe(10.5).stockid(1l)
				.build();
		StockPrice stockPrice = StockPrice.builder().date(LocalDate.now()).stock(stock).stockPriceId(1l).price(234.0)
				.build();
		CashBlance cashAccount = CashBlance.builder().balance(120.0).cashBalanceId(1l).user(user).build();
		StocksTransaction transaction = StocksTransaction.builder().shares(2).staus(Status.REJECTED).stock(stock)
				.totalCost(468.0).orderId(1l).transactionType(TransactionType.BUY).user(user).build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(stockRepository.findById(transactionDto.stockId())).thenReturn(Optional.of(stock));
		Mockito.when(stockPriceRepository.findByStockAndDate(stock, LocalDate.now())).thenReturn(stockPrice);
		Mockito.when(cashAccountRepository.findByUser(user)).thenReturn(cashAccount);
		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		assertThrows(InsufficientCashBalance.class,
				() -> transactionServiceImpl.buyStock(transactionDto, TransactionType.BUY));
	}
}
