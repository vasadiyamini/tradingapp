package com.example.trade.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.trade.dto.StockPricesDisplay;
import com.example.trade.entity.Stock;
import com.example.trade.entity.StockPrice;
import com.example.trade.exception.StockNotFoundException;
import com.example.trade.repository.StockPriceRepository;
import com.example.trade.repository.StockRepository;
import com.example.trade.serviceimp.PastStocksPriceServiceImp;

@ExtendWith(MockitoExtension.class)
class PastStocksPriceServiceImpTest {
	@Mock
	StockRepository stockRepository;

	@Mock
	StockPriceRepository stockPriceRepository;

	@InjectMocks
	PastStocksPriceServiceImp PastStocksPriceService;

	@Test
	void testGetStockPrices() {
		Stock mockStock = new Stock(1L, "ABC Company", 123L, 10.0, 100000L, 1.0);
		StockPrice mockStockPrice = new StockPrice(1L, mockStock, 100.0, LocalDate.now());
		when(stockRepository.findById(1L)).thenReturn(Optional.of(mockStock));

		when(stockPriceRepository.findTop5ByStockOrderByDateDesc(mockStock)).thenReturn(Arrays.asList(mockStockPrice));

		List<StockPricesDisplay> result = PastStocksPriceService.getStockPrices(1l);
		assertEquals(1, result.size());
	}

	@Test
	void testGetPastPriceStockStockNotFound() {
		when(stockRepository.findById(1L)).thenReturn(Optional.empty());

		assertThrows(StockNotFoundException.class, () -> PastStocksPriceService.getStockPrices(1l));
	}

}