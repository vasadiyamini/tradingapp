package com.example.trade.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.CashBalanceDto;
import com.example.trade.entity.CashBlance;
import com.example.trade.entity.User;
import com.example.trade.exception.ResourceNotFound;
import com.example.trade.repository.CashBalanceRepository;
import com.example.trade.serviceimp.CashBalanceServiceImplation;
import com.example.trade.util.SuccessCode;

@ExtendWith(SpringExtension.class)
class CashBalanceServiceImplationTest {

	@Mock
	private CashBalanceRepository cashBalanceRepository;

	@InjectMocks
	private CashBalanceServiceImplation cashBalanceService;

	@Test
	void testCashBalanced() {

		Long userId = 1L;
		CashBalanceDto cashBalanceDto = CashBalanceDto.builder().balance(1000.0).build();
		CashBlance cashBlance = new CashBlance();
		cashBlance.setBalance(500.0);
		cashBlance.setUser(new User());
		when(cashBalanceRepository.findById(userId)).thenReturn(Optional.of(cashBlance));

		ApiResponse response = cashBalanceService.cashBalanced(userId, cashBalanceDto);

		assertNotNull(response);
		assertEquals(SuccessCode.SUCCESS_CODE1, response.getCode());

		verify(cashBalanceRepository, times(1)).findById(userId);
		verify(cashBalanceRepository, times(1)).save(cashBlance);
	}

	@Test
	void testCashBalanced_ResourceNotFound() {

		Long userId = 1L;
		CashBalanceDto cashBalanceDto = CashBalanceDto.builder().balance(1000.0).build();
		when(cashBalanceRepository.findById(userId)).thenReturn(Optional.empty());

		assertThrows(ResourceNotFound.class, () -> cashBalanceService.cashBalanced(userId, cashBalanceDto));

		verify(cashBalanceRepository, times(1)).findById(userId);
		verify(cashBalanceRepository, never()).save(any());
	}
}