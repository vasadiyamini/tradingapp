package com.example.trade.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.trade.dto.StockPricesDisplay;
import com.example.trade.service.PastStocksPriceService;

@ExtendWith(SpringExtension.class)
class PastStockPricesControllerTest {
	@InjectMocks
	private PastStockPricesController pastStockPricesController;
	@Mock
	private PastStocksPriceService pastStocksPriceService;

	@Test
	void test() {
		Long stockId = 1l;
		StockPricesDisplay stockPricesDisplay = new StockPricesDisplay("hcl", 5000d, LocalDate.parse("2025-04-05"));
		List<StockPricesDisplay> stockPricesDisplays = Arrays.asList(stockPricesDisplay);
		when(pastStocksPriceService.getStockPrices(stockId)).thenReturn(stockPricesDisplays);

		ResponseEntity<List<StockPricesDisplay>> responseEntity = pastStockPricesController.getStockPrice(stockId);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

}
