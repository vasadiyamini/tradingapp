package com.example.trade.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.TransactionDto;
import com.example.trade.entity.TransactionType;
import com.example.trade.service.StockTransactionService;
import com.example.trade.util.SuccessCode;

@ExtendWith(SpringExtension.class)
class StockTransactionControllerTest {
	@Mock
	StockTransactionService stockTransactionService;
	@InjectMocks
	StockTransactionController stockTransactionController;

	@Test
	void testSuccess() {
		Mockito.when(
				stockTransactionService.buyStock(TransactionDto.builder().stockId(1L).build(), TransactionType.BUY))
				.thenReturn(ApiResponse.builder().code(SuccessCode.SUCCESS_CODE2).build());
		ResponseEntity<ApiResponse> responseEntity = stockTransactionController
				.buyStock(TransactionDto.builder().stockId(1L).build(), TransactionType.BUY);
		assertEquals(SuccessCode.SUCCESS_CODE2, responseEntity.getBody().getCode());
	}
}
