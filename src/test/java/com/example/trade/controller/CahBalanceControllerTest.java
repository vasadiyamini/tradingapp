package com.example.trade.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.trade.dto.ApiResponse;
import com.example.trade.dto.CashBalanceDto;
import com.example.trade.service.CashBalanceService;

@SpringBootTest
class CahBalanceControllerTest {

	@Mock
	private CashBalanceService cashBalanceService;

	@InjectMocks
	private CahBalanceController cashBalanceController;

	@Test
	void testCashBalanced() {

		Long userId = 1L;
		CashBalanceDto cashBalanceDto = CashBalanceDto.builder().balance(1000.0).build();
		ApiResponse expectedResponse = new ApiResponse();
		expectedResponse.setCode("SUCCESS_CODE");
		expectedResponse.setMessage("Success Message");

		when(cashBalanceService.cashBalanced(Mockito.anyLong(), Mockito.any())).thenReturn(expectedResponse);

		ResponseEntity<ApiResponse> responseEntity = cashBalanceController.creadiBalnced(userId, cashBalanceDto);

		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals(expectedResponse, responseEntity.getBody());

	}
}