package com.example.trade.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.trade.dto.StockPricesDisplay;
import com.example.trade.service.CurrentStockPriceService;

@ExtendWith(SpringExtension.class)
class CurrentStockPriceControllerTest {
	@InjectMocks
	CurrentStockPriceController currentStockPriceController;
	@Mock
	CurrentStockPriceService currentStockPriceService;

	@Test
	void testGetCurrentPriceStock() {
		Long stockId = 1l;
		StockPricesDisplay stockPricesDisplay = new StockPricesDisplay("hcl", 5000d, LocalDate.parse("2025-04-05"));
		when(currentStockPriceService.getCurrentPriceStock(stockId)).thenReturn(stockPricesDisplay);
		ResponseEntity<StockPricesDisplay> responseEntity = currentStockPriceController.getCurrentPriceStock(stockId);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(stockPricesDisplay, responseEntity.getBody());
	}

}
