package com.example.trade.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.trade.dto.OrderHistoryDto;
import com.example.trade.dto.TradeHistoryDto;
import com.example.trade.dto.UserHistoryDto;
import com.example.trade.service.UserService;

@ExtendWith(SpringExtension.class)
class UserServiceControllerTest {
	@Mock
	UserService userService;
	@InjectMocks
	UserController userController;

	@Test
	void testSuccess() {
		UserHistoryDto userHistoryDto = new UserHistoryDto(1000D,
				Arrays.asList(OrderHistoryDto.builder().CompanyName("HCL").build()),
				Arrays.asList(TradeHistoryDto.builder().companyName("HCL").build()));
		Mockito.when(userService.searchUserHistory(1L)).thenReturn(userHistoryDto);
		ResponseEntity<UserHistoryDto> responseEntity = userController.searchHistory(1L);
		assertEquals(1000D, responseEntity.getBody().cashBalance());
	}
}
